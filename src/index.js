import Components from "./components";
import Vue from "vue";

const JbModal = {
  install(Vue, options) {
    Components(Vue);

    // Vue.mixin({
    //   mounted() {
    //     console.log("Mounted");
    //   }
    // });
  }
};

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(JbModal);
}

export default JbModal;
