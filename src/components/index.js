import JbModal from "./Modal.vue";
import InputTag from "./InputTag.vue";
import InputMulti from "./InputMulti.vue";
import InputDropdown from "./InputDropdown.vue";

export default function(Vue) {
  Vue.component("jb-modal", JbModal);
  Vue.component("jb-input-tag", InputTag);
  Vue.component("jb-input-multi", InputMulti);
  Vue.component("jb-dropdown", InputDropdown);
}
