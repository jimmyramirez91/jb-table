import Vue from "vue";

const DeepVue = {
  deepFind(obj, path) {
    var paths = path.split("."),
      current = obj,
      i;

    for (i = 0; i < paths.length; ++i) {
      if (current[paths[i]] == undefined) {
        return undefined;
      } else {
        current = current[paths[i]];
      }
    }
    return current;
  },
  isNumberLike(value) {
    return String(value).match(/^\d+$/);
  },
  toPath(pathString) {
    if (Array.isArray(pathString)) return pathString;
    if (typeof pathString === "number") return [pathString];
    pathString = String(pathString);

    // taken from lodash - https://github.com/lodash/lodash
    let pathRx = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(\.|\[\])(?:\4|$))/g;
    let pathArray = [];

    pathString.replace(pathRx, (match, number, quote, string) => {
      pathArray.push(
        quote ? string : number !== undefined ? Number(number) : match
      );
      return pathArray[pathArray.length - 1];
    });
    return pathArray;
  },
  hasOwnProperty(object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  },
  vueSet(ve, obj, path, value) {
    let fields = Array.isArray(path) ? path : this.toPath(path);
    let prop = fields.shift();

    if (!fields.length) return ve.$set(obj, prop, value);

    if (!this.hasOwnProperty(obj, prop) || obj[prop] === null) {
      const objVal =
        fields.length >= 1 && this.isNumberLike(fields[0]) ? [] : {};
        ve.$set(ve,obj, prop, objVal);
    }
    this.vueSet(ve, obj[prop], fields, value);
  }
};

export default DeepVue;
